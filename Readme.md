## Content
This project aims to simplify the build process of the original nand2tetris-tools-suite by using gradle. The project's file structure has remained untouched as much as possible so that descriptions in the original `readme.txt` from the nand2tetris team still apply. The only change is, that I've moved source files to a `src` subfolder in each project (IntelliJ Idea removes source folders if they are equal to the projects' root folder).

## Compilation Instructions:
* For building, execute `./gradlew build` from the project root. The software is built into `InstallDir` (see _Directory Structure and Compilation Instructions_ below) and can be executed from here using the scripts provided by the nand2tetris developers
* The tasks `runCpuEmulator`, `runHardwareSimulator` and `runVMEmulator` can be used to run the respective programs (e.g. for debugging purposes) without needing to execute a script manually